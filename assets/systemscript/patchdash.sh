#patching /usr/share/unity8/Launcher/Launcher.qml

if [ -z $(grep "launchermodular" /usr/share/unity8/Launcher/Launcher.qml ) ]
then
mount -o rw,remount /
sed -i '/property bool drawerShown/a\property bool launchermodular: true\
readonly property string homeApp: "launchermodular.ubuntouchfr_launchermodular"\
' /usr/share/unity8/Launcher/Launcher.qml

sed -i '/function toggleDrawer/a\shell.startApp(root.homeApp);\
if(LauncherModel.search(root.homeApp) >= 0)\
	return;\
else\
	root.launchermodular = false;\
' /usr/share/unity8/Launcher/Launcher.qml

sed -i '/        width: Math.min(root.width, units.gu(81))/c\        width: Math.min(root.width, units.gu(8))
' /usr/share/unity8/Launcher/Launcher.qml

mount -o ro,remount /
fi
